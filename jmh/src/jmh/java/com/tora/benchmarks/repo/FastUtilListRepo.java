package com.tora.benchmarks.repo;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class FastUtilListRepo<T> implements InMemoryRepo<T> {

    private ObjectArrayList<T> arrayList;

    public FastUtilListRepo() {
        arrayList = new ObjectArrayList<>();
    }

    @Override
    public void add(T item) {
        arrayList.add(item);
    }

    @Override
    public void remove(T item) {
        arrayList.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return arrayList.contains(item);
    }

    @Override
    public void clear() {
        arrayList.clear();
    }
}

