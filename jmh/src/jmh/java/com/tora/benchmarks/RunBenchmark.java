package com.tora.benchmarks;

import com.tora.benchmarks.repo.ArrayListRepo;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class RunBenchmark
{
    public static void main(String[] args) throws Exception
    {
        org.openjdk.jmh.Main.main(args);
        Options opt = new OptionsBuilder()
                .include(ArrayListRepo.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();

    }
}
