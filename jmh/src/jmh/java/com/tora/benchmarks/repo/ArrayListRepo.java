package com.tora.benchmarks.repo;

import java.util.ArrayList;
import java.util.List;

public class ArrayListRepo<T> implements InMemoryRepo<T> {

    private List<T> list;

    public ArrayListRepo() {
        this.list = new ArrayList<T>();
    }

    @Override
    public void add(T item) {
        list.add(item);
    }

    @Override
    public void remove(T item) {
        list.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return list.contains(item);
    }

    @Override
    public void clear() {
        list.clear();
    }
}

