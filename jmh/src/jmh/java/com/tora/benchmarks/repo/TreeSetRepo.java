package com.tora.benchmarks.repo;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetRepo<T extends Comparable<T>> implements InMemoryRepo<T> {

    private final Set<T> set;

    public TreeSetRepo() {
        this.set = new TreeSet<T>();
    }

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void clear() {
        set.clear();
    }
}

