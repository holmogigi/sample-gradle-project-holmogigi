package com.tora.benchmarks.repo;

public interface InMemoryRepo<T>
{
    void add(T item);
    void remove(T item);
    boolean contains(T item);

    void clear();
}

