package org.example.controller;
import org.example.model.*;

public class CalculatorController
{
    public static double Calculate(String operator, int a, int b)
    {
        if (operator.equals("+")) {
            return Add.exe(a, b);
        } else if (operator.equals("-")) {
            return Sub.exe(a, b);
        } else if (operator.equals("/")) {
            return Div.exe(a, b);
        } else if (operator.equals("*")) {
            return Mul.exe(a, b);
        } else if (operator.equals("min")) {
            return Min.exe(a, b);
        } else if (operator.equals("max")) {
            return Max.exe(a, b);
        } else if (operator.equals("sqrt")) {
            return SQR.exe(a);
        } else {
            return 0;
        }
    }
}
