10:25:34: Executing ':jmh-benchmarks:Benchmarks.main()'...

> Task :jmh-benchmarks:compileJava UP-TO-DATE
> Task :jmh-benchmarks:processResources NO-SOURCE
> Task :jmh-benchmarks:classes UP-TO-DATE
> Task :jmh-benchmarks:compileTestJava NO-SOURCE
> Task :jmh-benchmarks:processTestResources NO-SOURCE
> Task :jmh-benchmarks:testClasses UP-TO-DATE
> Task :jmh-benchmarks:compileJmhJava
> Task :jmh-benchmarks:processJmhResources NO-SOURCE
> Task :jmh-benchmarks:jmhClasses

> Task :jmh-benchmarks:Benchmarks.main()
# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.arrayListAdd

# Run progress: 0.00% complete, ETA 00:06:24
# Fork: 1 of 1
# Warmup Iteration   1: 4470911.364 ops/s
# Warmup Iteration   2: 4591047.346 ops/s
# Warmup Iteration   3: 6024863.776 ops/s
Iteration   1: 6231485.297 ops/s
Iteration   2: 6230356.735 ops/s
Iteration   3: 6225349.254 ops/s
Iteration   4: 6226849.591 ops/s
Iteration   5: 6244681.946 ops/s


Result "benchmarks.Benchmarks.arrayListAdd":
  6231744.565 �(99.9%) 29466.227 ops/s [Average]
  (min, avg, max) = (6225349.254, 6231744.565, 6244681.946), stdev = 7652.286
  CI (99.9%): [6202278.338, 6261210.792] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.arrayListContains

# Run progress: 4.17% complete, ETA 00:06:21
# Fork: 1 of 1
# Warmup Iteration   1: 32645951.220 ops/s
# Warmup Iteration   2: 32747940.377 ops/s
# Warmup Iteration   3: 32663664.364 ops/s
Iteration   1: 32492965.734 ops/s
Iteration   2: 32833012.380 ops/s
Iteration   3: 32778700.303 ops/s
Iteration   4: 32831814.717 ops/s
Iteration   5: 32890195.251 ops/s


Result "benchmarks.Benchmarks.arrayListContains":
  32765337.677 �(99.9%) 605646.162 ops/s [Average]
  (min, avg, max) = (32492965.734, 32765337.677, 32890195.251), stdev = 157284.385
  CI (99.9%): [32159691.515, 33370983.839] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.arrayListRemove

# Run progress: 8.33% complete, ETA 00:06:03
# Fork: 1 of 1
# Warmup Iteration   1: 32603075.065 ops/s
# Warmup Iteration   2: 32768236.795 ops/s
# Warmup Iteration   3: 32769185.702 ops/s
Iteration   1: 32719775.682 ops/s
Iteration   2: 32568591.612 ops/s
Iteration   3: 32734076.385 ops/s
Iteration   4: 32706062.438 ops/s
Iteration   5: 32364962.106 ops/s


Result "benchmarks.Benchmarks.arrayListRemove":
  32618693.644 �(99.9%) 602887.047 ops/s [Average]
  (min, avg, max) = (32364962.106, 32618693.644, 32734076.385), stdev = 156567.852
  CI (99.9%): [32015806.597, 33221580.691] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.concurrentHashMapAdd

# Run progress: 12.50% complete, ETA 00:05:46
# Fork: 1 of 1
# Warmup Iteration   1: 8389190.450 ops/s
# Warmup Iteration   2: 8704999.877 ops/s
# Warmup Iteration   3: 8768655.682 ops/s
Iteration   1: 8634542.423 ops/s
Iteration   2: 8702390.117 ops/s
Iteration   3: 8612825.418 ops/s
Iteration   4: 8757925.111 ops/s
Iteration   5: 8773902.440 ops/s


Result "benchmarks.Benchmarks.concurrentHashMapAdd":
  8696317.102 �(99.9%) 276596.374 ops/s [Average]
  (min, avg, max) = (8612825.418, 8696317.102, 8773902.440), stdev = 71831.200
  CI (99.9%): [8419720.727, 8972913.476] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.concurrentHashMapContains

# Run progress: 16.67% complete, ETA 00:05:29
# Fork: 1 of 1
# Warmup Iteration   1: 32583403.442 ops/s
# Warmup Iteration   2: 32581330.627 ops/s
# Warmup Iteration   3: 32858136.796 ops/s
Iteration   1: 33084015.620 ops/s
Iteration   2: 33019028.493 ops/s
Iteration   3: 33148995.176 ops/s
Iteration   4: 33167885.494 ops/s
Iteration   5: 33061205.626 ops/s


Result "benchmarks.Benchmarks.concurrentHashMapContains":
  33096226.082 �(99.9%) 237795.923 ops/s [Average]
  (min, avg, max) = (33019028.493, 33096226.082, 33167885.494), stdev = 61754.846
  CI (99.9%): [32858430.159, 33334022.005] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.concurrentHashMapRemove

# Run progress: 20.83% complete, ETA 00:05:13
# Fork: 1 of 1
# Warmup Iteration   1: 28585100.193 ops/s
# Warmup Iteration   2: 28356694.905 ops/s
# Warmup Iteration   3: 28724714.216 ops/s
Iteration   1: 28875974.335 ops/s
Iteration   2: 28821974.222 ops/s
Iteration   3: 28854884.227 ops/s
Iteration   4: 28614635.614 ops/s
Iteration   5: 28879784.588 ops/s


Result "benchmarks.Benchmarks.concurrentHashMapRemove":
  28809450.597 �(99.9%) 428556.998 ops/s [Average]
  (min, avg, max) = (28614635.614, 28809450.597, 28879784.588), stdev = 111294.892
  CI (99.9%): [28380893.599, 29238007.596] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.hashSetAdd

# Run progress: 25.00% complete, ETA 00:04:56
# Fork: 1 of 1
# Warmup Iteration   1: 10461422.092 ops/s
# Warmup Iteration   2: 10733985.649 ops/s
# Warmup Iteration   3: 10672555.656 ops/s
Iteration   1: 10417540.522 ops/s
Iteration   2: 10729167.319 ops/s
Iteration   3: 10468347.407 ops/s
Iteration   4: 10683026.141 ops/s
Iteration   5: 10762020.208 ops/s


Result "benchmarks.Benchmarks.hashSetAdd":
  10612020.320 �(99.9%) 608016.280 ops/s [Average]
  (min, avg, max) = (10417540.522, 10612020.320, 10762020.208), stdev = 157899.897
  CI (99.9%): [10004004.039, 11220036.600] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.hashSetContains

# Run progress: 29.17% complete, ETA 00:04:39
# Fork: 1 of 1
# Warmup Iteration   1: 32228693.103 ops/s
# Warmup Iteration   2: 32272389.266 ops/s
# Warmup Iteration   3: 32441891.659 ops/s
Iteration   1: 32513325.879 ops/s
Iteration   2: 32490845.119 ops/s
Iteration   3: 32597588.463 ops/s
Iteration   4: 32505420.991 ops/s
Iteration   5: 32589294.250 ops/s


Result "benchmarks.Benchmarks.hashSetContains":
  32539294.940 �(99.9%) 193178.861 ops/s [Average]
  (min, avg, max) = (32490845.119, 32539294.940, 32597588.463), stdev = 50167.937
  CI (99.9%): [32346116.079, 32732473.801] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.hashSetRemove

# Run progress: 33.33% complete, ETA 00:04:23
# Fork: 1 of 1
# Warmup Iteration   1: 32467148.507 ops/s
# Warmup Iteration   2: 32579649.239 ops/s
# Warmup Iteration   3: 32679495.475 ops/s
Iteration   1: 32559000.525 ops/s
Iteration   2: 32681881.635 ops/s
Iteration   3: 32860017.343 ops/s
Iteration   4: 32771204.982 ops/s
Iteration   5: 32780520.237 ops/s


Result "benchmarks.Benchmarks.hashSetRemove":
  32730524.944 �(99.9%) 442025.889 ops/s [Average]
  (min, avg, max) = (32559000.525, 32730524.944, 32860017.343), stdev = 114792.720
  CI (99.9%): [32288499.055, 33172550.834] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.intArrayListAdd

# Run progress: 37.50% complete, ETA 00:04:06
# Fork: 1 of 1
# Warmup Iteration   1: 24525261.047 ops/s
# Warmup Iteration   2: 24805119.656 ops/s
# Warmup Iteration   3: 25099968.522 ops/s
Iteration   1: 25155102.318 ops/s
Iteration   2: 25082824.895 ops/s
Iteration   3: 25165881.071 ops/s
Iteration   4: 25088639.528 ops/s
Iteration   5: 25101874.468 ops/s


Result "benchmarks.Benchmarks.intArrayListAdd":
  25118864.456 �(99.9%) 149442.699 ops/s [Average]
  (min, avg, max) = (25082824.895, 25118864.456, 25165881.071), stdev = 38809.794
  CI (99.9%): [24969421.757, 25268307.155] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.intArrayListContains

# Run progress: 41.67% complete, ETA 00:03:50
# Fork: 1 of 1
# Warmup Iteration   1: 32936534.274 ops/s
# Warmup Iteration   2: 33058254.104 ops/s
# Warmup Iteration   3: 33102474.865 ops/s
Iteration   1: 33108834.859 ops/s
Iteration   2: 33058194.978 ops/s
Iteration   3: 33156995.404 ops/s
Iteration   4: 33129168.527 ops/s
Iteration   5: 33166317.138 ops/s


Result "benchmarks.Benchmarks.intArrayListContains":
  33123902.181 �(99.9%) 166363.695 ops/s [Average]
  (min, avg, max) = (33058194.978, 33123902.181, 33166317.138), stdev = 43204.123
  CI (99.9%): [32957538.486, 33290265.876] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.intArrayListRemove

# Run progress: 45.83% complete, ETA 00:03:33
# Fork: 1 of 1
# Warmup Iteration   1: 32930555.624 ops/s
# Warmup Iteration   2: 33050125.434 ops/s
# Warmup Iteration   3: 33149650.077 ops/s
Iteration   1: 33083153.146 ops/s
Iteration   2: 33140140.934 ops/s
Iteration   3: 32954693.358 ops/s
Iteration   4: 33019107.819 ops/s
Iteration   5: 33172835.011 ops/s


Result "benchmarks.Benchmarks.intArrayListRemove":
  33073986.053 �(99.9%) 341569.632 ops/s [Average]
  (min, avg, max) = (32954693.358, 33073986.053, 33172835.011), stdev = 88704.549
  CI (99.9%): [32732416.422, 33415555.685] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.intIntHashMapAdd

# Run progress: 50.00% complete, ETA 00:03:17
# Fork: 1 of 1
# Warmup Iteration   1: 22908828.567 ops/s
# Warmup Iteration   2: 22959125.040 ops/s
# Warmup Iteration   3: 23221777.307 ops/s
Iteration   1: 23224964.916 ops/s
Iteration   2: 23167679.180 ops/s
Iteration   3: 23224231.994 ops/s
Iteration   4: 23163194.427 ops/s
Iteration   5: 23234055.733 ops/s


Result "benchmarks.Benchmarks.intIntHashMapAdd":
  23202825.250 �(99.9%) 132408.480 ops/s [Average]
  (min, avg, max) = (23163194.427, 23202825.250, 23234055.733), stdev = 34386.062
  CI (99.9%): [23070416.770, 23335233.730] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.intIntHashMapContains

# Run progress: 54.17% complete, ETA 00:03:01
# Fork: 1 of 1
# Warmup Iteration   1: 26218447.535 ops/s
# Warmup Iteration   2: 26252516.803 ops/s
# Warmup Iteration   3: 25885913.723 ops/s
Iteration   1: 26205585.128 ops/s
Iteration   2: 26114127.195 ops/s
Iteration   3: 26038899.260 ops/s
Iteration   4: 26194160.220 ops/s
Iteration   5: 26073173.669 ops/s


Result "benchmarks.Benchmarks.intIntHashMapContains":
  26125189.094 �(99.9%) 282271.137 ops/s [Average]
  (min, avg, max) = (26038899.260, 26125189.094, 26205585.128), stdev = 73304.918
  CI (99.9%): [25842917.957, 26407460.231] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.intIntHashMapRemove

# Run progress: 58.33% complete, ETA 00:02:44
# Fork: 1 of 1
# Warmup Iteration   1: 32364955.282 ops/s
# Warmup Iteration   2: 32515493.011 ops/s
# Warmup Iteration   3: 32531070.476 ops/s
Iteration   1: 32580345.083 ops/s
Iteration   2: 32449153.698 ops/s
Iteration   3: 32609036.817 ops/s
Iteration   4: 32601196.197 ops/s
Iteration   5: 32571281.533 ops/s


Result "benchmarks.Benchmarks.intIntHashMapRemove":
  32562202.665 �(99.9%) 250333.139 ops/s [Average]
  (min, avg, max) = (32449153.698, 32562202.665, 32609036.817), stdev = 65010.721
  CI (99.9%): [32311869.526, 32812535.804] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.longOpenHashSetAdd

# Run progress: 62.50% complete, ETA 00:02:28
# Fork: 1 of 1
# Warmup Iteration   1: 22107747.626 ops/s
# Warmup Iteration   2: 19897700.132 ops/s
# Warmup Iteration   3: 22455405.050 ops/s
Iteration   1: 22498102.769 ops/s
Iteration   2: 22490392.906 ops/s
Iteration   3: 22489920.156 ops/s
Iteration   4: 22464132.016 ops/s
Iteration   5: 22479463.423 ops/s


Result "benchmarks.Benchmarks.longOpenHashSetAdd":
  22484402.254 �(99.9%) 50548.919 ops/s [Average]
  (min, avg, max) = (22464132.016, 22484402.254, 22498102.769), stdev = 13127.394
  CI (99.9%): [22433853.336, 22534951.173] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.longOpenHashSetContains

# Run progress: 66.67% complete, ETA 00:02:11
# Fork: 1 of 1
# Warmup Iteration   1: 32238560.823 ops/s
# Warmup Iteration   2: 32557927.252 ops/s
# Warmup Iteration   3: 32565513.680 ops/s
Iteration   1: 32614066.192 ops/s
Iteration   2: 32628149.308 ops/s
Iteration   3: 32592929.645 ops/s
Iteration   4: 32489002.071 ops/s
Iteration   5: 32561361.062 ops/s


Result "benchmarks.Benchmarks.longOpenHashSetContains":
  32577101.656 �(99.9%) 212938.068 ops/s [Average]
  (min, avg, max) = (32489002.071, 32577101.656, 32628149.308), stdev = 55299.340
  CI (99.9%): [32364163.588, 32790039.724] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.longOpenHashSetRemove

# Run progress: 70.83% complete, ETA 00:01:55
# Fork: 1 of 1
# Warmup Iteration   1: 32417368.631 ops/s
# Warmup Iteration   2: 32504152.240 ops/s
# Warmup Iteration   3: 32580492.750 ops/s
Iteration   1: 32579735.521 ops/s
Iteration   2: 32562530.416 ops/s
Iteration   3: 32560993.493 ops/s
Iteration   4: 32604327.674 ops/s
Iteration   5: 32576093.998 ops/s


Result "benchmarks.Benchmarks.longOpenHashSetRemove":
  32576736.220 �(99.9%) 67257.835 ops/s [Average]
  (min, avg, max) = (32560993.493, 32576736.220, 32604327.674), stdev = 17466.646
  CI (99.9%): [32509478.385, 32643994.055] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.mutableListAdd

# Run progress: 75.00% complete, ETA 00:01:38
# Fork: 1 of 1
# Warmup Iteration   1: 23664602.026 ops/s
# Warmup Iteration   2: 17557459.252 ops/s
# Warmup Iteration   3: 16950147.660 ops/s
Iteration   1: 17004407.859 ops/s
Iteration   2: 17111613.935 ops/s
Iteration   3: 17075375.321 ops/s
Iteration   4: 16982248.683 ops/s
Iteration   5: 17052692.036 ops/s


Result "benchmarks.Benchmarks.mutableListAdd":
  17045267.567 �(99.9%) 201969.262 ops/s [Average]
  (min, avg, max) = (16982248.683, 17045267.567, 17111613.935), stdev = 52450.776
  CI (99.9%): [16843298.305, 17247236.829] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.mutableListContains

# Run progress: 79.17% complete, ETA 00:01:22
# Fork: 1 of 1
# Warmup Iteration   1: 32575967.818 ops/s
# Warmup Iteration   2: 32868707.746 ops/s
# Warmup Iteration   3: 32987107.160 ops/s
Iteration   1: 32883191.561 ops/s
Iteration   2: 33062337.753 ops/s
Iteration   3: 32870347.633 ops/s
Iteration   4: 32880996.409 ops/s
Iteration   5: 32934201.801 ops/s


Result "benchmarks.Benchmarks.mutableListContains":
  32926215.031 �(99.9%) 308112.234 ops/s [Average]
  (min, avg, max) = (32870347.633, 32926215.031, 33062337.753), stdev = 80015.769
  CI (99.9%): [32618102.797, 33234327.265] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.mutableListRemove

# Run progress: 83.33% complete, ETA 00:01:05
# Fork: 1 of 1
# Warmup Iteration   1: 32398703.956 ops/s
# Warmup Iteration   2: 32787724.561 ops/s
# Warmup Iteration   3: 33111921.971 ops/s
Iteration   1: 32896515.926 ops/s
Iteration   2: 33051373.773 ops/s
Iteration   3: 32834477.122 ops/s
Iteration   4: 32992547.519 ops/s
Iteration   5: 33059789.130 ops/s


Result "benchmarks.Benchmarks.mutableListRemove":
  32966940.694 �(99.9%) 379790.107 ops/s [Average]
  (min, avg, max) = (32834477.122, 32966940.694, 33059789.130), stdev = 98630.285
  CI (99.9%): [32587150.587, 33346730.801] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.treeSetAdd

# Run progress: 87.50% complete, ETA 00:00:49
# Fork: 1 of 1
# Warmup Iteration   1: 10064025.590 ops/s
# Warmup Iteration   2: 10354359.893 ops/s
# Warmup Iteration   3: 10175633.840 ops/s
Iteration   1: 10194180.397 ops/s
Iteration   2: 10163175.291 ops/s
Iteration   3: 10120505.936 ops/s
Iteration   4: 10153291.033 ops/s
Iteration   5: 10182552.186 ops/s


Result "benchmarks.Benchmarks.treeSetAdd":
  10162740.968 �(99.9%) 109831.304 ops/s [Average]
  (min, avg, max) = (10120505.936, 10162740.968, 10194180.397), stdev = 28522.841
  CI (99.9%): [10052909.664, 10272572.273] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.treeSetContains

# Run progress: 91.67% complete, ETA 00:00:33
# Fork: 1 of 1
# Warmup Iteration   1: 32443582.134 ops/s
# Warmup Iteration   2: 32505824.926 ops/s
# Warmup Iteration   3: 32794741.763 ops/s
Iteration   1: 32855112.183 ops/s
Iteration   2: 32817055.385 ops/s
Iteration   3: 32714024.938 ops/s
Iteration   4: 32766830.858 ops/s
Iteration   5: 32747749.072 ops/s


Result "benchmarks.Benchmarks.treeSetContains":
  32780154.487 �(99.9%) 215954.532 ops/s [Average]
  (min, avg, max) = (32714024.938, 32780154.487, 32855112.183), stdev = 56082.706
  CI (99.9%): [32564199.955, 32996109.019] (assumes normal distribution)


# JMH version: 1.36
# VM version: JDK 16.0.2, OpenJDK 64-Bit Server VM, 16.0.2+7-67
# VM invoker: C:\Program Files (x86)\jdk-16.0.2\bin\java.exe
# VM options: -Dfile.encoding=windows-1252 -Duser.country=US -Duser.language=en -Duser.variant
# Blackhole mode: full + dont-inline hint (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
# Warmup: 3 iterations, 2 s each
# Measurement: 5 iterations, 2 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: benchmarks.Benchmarks.treeSetRemove

# Run progress: 95.83% complete, ETA 00:00:16
# Fork: 1 of 1
# Warmup Iteration   1: 32340564.357 ops/s
# Warmup Iteration   2: 32654003.078 ops/s
# Warmup Iteration   3: 32825221.497 ops/s
Iteration   1: 32697465.391 ops/s
Iteration   2: 32796767.042 ops/s
Iteration   3: 32848420.240 ops/s
Iteration   4: 32713195.135 ops/s
Iteration   5: 32850072.371 ops/s


Result "benchmarks.Benchmarks.treeSetRemove":
  32781184.036 �(99.9%) 279937.060 ops/s [Average]
  (min, avg, max) = (32697465.391, 32781184.036, 32850072.371), stdev = 72698.766
  CI (99.9%): [32501246.976, 33061121.096] (assumes normal distribution)


# Run complete. Total time: 00:06:35

REMEMBER: The numbers below are just data. To gain reusable insights, you need to follow up on
why the numbers are the way they are. Use profilers (see -prof, -lprof), design factorial
experiments, perform baseline and negative tests that provide experimental control, make sure
the benchmarking environment is safe on JVM/OS/HW level, ask for reviews from the domain experts.
Do not assume the numbers tell you what you want them to tell.

Benchmark                              Mode  Cnt         Score        Error  Units
Benchmarks.arrayListAdd               thrpt    5   6231744.565 �  29466.227  ops/s
Benchmarks.arrayListContains          thrpt    5  32765337.677 � 605646.162  ops/s
Benchmarks.arrayListRemove            thrpt    5  32618693.644 � 602887.047  ops/s
Benchmarks.concurrentHashMapAdd       thrpt    5   8696317.102 � 276596.374  ops/s
Benchmarks.concurrentHashMapContains  thrpt    5  33096226.082 � 237795.923  ops/s
Benchmarks.concurrentHashMapRemove    thrpt    5  28809450.597 � 428556.998  ops/s
Benchmarks.hashSetAdd                 thrpt    5  10612020.320 � 608016.280  ops/s
Benchmarks.hashSetContains            thrpt    5  32539294.940 � 193178.861  ops/s
Benchmarks.hashSetRemove              thrpt    5  32730524.944 � 442025.889  ops/s
Benchmarks.intArrayListAdd            thrpt    5  25118864.456 � 149442.699  ops/s
Benchmarks.intArrayListContains       thrpt    5  33123902.181 � 166363.695  ops/s
Benchmarks.intArrayListRemove         thrpt    5  33073986.053 � 341569.632  ops/s
Benchmarks.intIntHashMapAdd           thrpt    5  23202825.250 � 132408.480  ops/s
Benchmarks.intIntHashMapContains      thrpt    5  26125189.094 � 282271.137  ops/s
Benchmarks.intIntHashMapRemove        thrpt    5  32562202.665 � 250333.139  ops/s
Benchmarks.longOpenHashSetAdd         thrpt    5  22484402.254 �  50548.919  ops/s
Benchmarks.longOpenHashSetContains    thrpt    5  32577101.656 � 212938.068  ops/s
Benchmarks.longOpenHashSetRemove      thrpt    5  32576736.220 �  67257.835  ops/s
Benchmarks.mutableListAdd             thrpt    5  17045267.567 � 201969.262  ops/s
Benchmarks.mutableListContains        thrpt    5  32926215.031 � 308112.234  ops/s
Benchmarks.mutableListRemove          thrpt    5  32966940.694 � 379790.107  ops/s
Benchmarks.treeSetAdd                 thrpt    5  10162740.968 � 109831.304  ops/s
Benchmarks.treeSetContains            thrpt    5  32780154.487 � 215954.532  ops/s
Benchmarks.treeSetRemove              thrpt    5  32781184.036 � 279937.060  ops/s

BUILD SUCCESSFUL in 6m 39s
3 actionable tasks: 2 executed, 1 up-to-date
10:32:14: Execution finished ':jmh-benchmarks:Benchmarks.main()'.
