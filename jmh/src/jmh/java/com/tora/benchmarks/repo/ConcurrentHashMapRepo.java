package com.tora.benchmarks.repo;

import java.util.AbstractMap;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapRepo<T> implements InMemoryRepo<T> {

    private final AbstractMap<Integer, T> map;

    public ConcurrentHashMapRepo() {
        map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T item) {
        map.put(item.hashCode(), item);
    }

    @Override
    public void remove(T item) {
        map.remove(item.hashCode());
    }

    @Override
    public boolean contains(T item) {
        return map.containsKey(item.hashCode());
    }

    @Override
    public void clear() {
        map.clear();
    }
}


