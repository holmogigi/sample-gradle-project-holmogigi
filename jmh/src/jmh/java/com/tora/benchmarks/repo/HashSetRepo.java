package com.tora.benchmarks.repo;

import java.util.HashSet;
import java.util.Set;

public class HashSetRepo<T> implements InMemoryRepo<T>
{
    private final Set<T> set;

    public HashSetRepo() {
        this.set = new HashSet<T>();
    }

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void clear() {
        set.clear();
    }
}


